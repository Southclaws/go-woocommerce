package util

import (
	"github.com/pkg/errors"
)

// AssertStatus generates an error message if the expected status does not match
func AssertStatus(got, want int) (err error) {
	if got != want {
		return errors.Errorf("expected status %d, got %d", want, got)
	}
	return
}

// AssertStatuses does the same as AssertStatus but for multiple expected
func AssertStatuses(got int, wants ...int) (err error) {
	for _, want := range wants {
		if got != want {
			return errors.Errorf("expected status %d, got %d", want, got)
		}
	}
	return
}

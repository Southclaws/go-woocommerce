package woocommerce

import (
	"os"
	"testing"

	"github.com/joeshaw/iso8601"
	_ "github.com/joho/godotenv/autoload"
	"github.com/kelseyhightower/envconfig"
)

var client *Client

func TestMain(m *testing.M) {
	config := Config{}
	envconfig.MustProcess("TEST", &config)

	var err error
	client, err = New(config)
	if err != nil {
		panic(err)
	}
	client.r.SetDebug(true)

	ret := m.Run()

	os.Exit(ret)
}

// helpers for creating pointers from literals
func pS(s string) *string             { return &s }
func pI(i int) *int                   { return &i }
func pF(f float64) *float64           { return &f }
func pT(t iso8601.Time) *iso8601.Time { return &t }

func pStatus(s Status) *Status { return &s }
func pTopic(t Topic) *Topic    { return &t }

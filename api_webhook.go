package woocommerce

import (
	"fmt"
	"net/url"

	"github.com/Southclaws/qstring"
	"github.com/pkg/errors"
)

// WebhookCreate creates a webhook
func (c *Client) WebhookCreate(name string, status Status, topic Topic, url string) (result Webhook, err error) {
	err = c.Create("webhooks", nil, Webhook{
		Name:        &name,
		Status:      &status,
		Topic:       &topic,
		DeliveryURL: &url,
	}, &result)
	return
}

// WebhookRetrieve retrieves a webhook by its ID
func (c *Client) WebhookRetrieve(id int) (result Webhook, err error) {
	err = c.Retrieve(fmt.Sprint("webhooks/", id), nil, &result)
	return
}

// WebhookListParams defines query parameters for listing webhooks
type WebhookListParams struct {
	Context string `qstring:",omitempty"`         // Scope under which the request is made; determines fields present in response. Options: view and edit. Default is view.
	Page    int    `qstring:",omitempty"`         // Current page of the collection. Default is 1.
	PerPage int    `qstring:"per_page,omitempty"` // Maximum number of items to be returned in result set. Default is 10.
	Search  string `qstring:",omitempty"`         // Limit results to those matching a string.
	After   string `qstring:",omitempty"`         // Limit response to resources published after a given ISO8601 compliant date.
	Before  string `qstring:",omitempty"`         // Limit response to resources published before a given ISO8601 compliant date.
	Exclude []int  `qstring:",omitempty,comma"`   // Ensure result set excludes specific IDs.
	Include []int  `qstring:",omitempty,comma"`   // Limit result set to specific ids.
	Offset  int    `qstring:",omitempty"`         // Offset the result set by a specific number of items.
	Order   string `qstring:",omitempty"`         // Order sort attribute ascending or descending. Options: asc and desc. Default is desc.
	OrderBy string `qstring:"orderby,omitempty"`  // Sort collection by object attribute. Options: date, id, include, title and slug. Default is date.
	Status  string `qstring:",omitempty"`         // Limit result set to webhooks assigned a specific status. Options: all, active, paused and disabled. Default is all.
}

// WebhookList gets a list of all webhooks
func (c *Client) WebhookList(query WebhookListParams) (result []Webhook, err error) {
	values, err := qstring.Marshal(&query)
	if err != nil {
		err = errors.Wrap(err, "failed to encode query parameters")
		return
	}
	err = c.Retrieve(fmt.Sprint("webhooks"), values, &result)
	return
}

// WebhookUpdate updates a webhook
func (c *Client) WebhookUpdate(id int, webhook Webhook) (result Webhook, err error) {
	err = c.Update(fmt.Sprintf("webhooks/%d", id), nil, webhook, &result)
	return
}

// WebhookDelete deletes a webhook by its ID
func (c *Client) WebhookDelete(id int) (result Webhook, err error) {
	err = c.Delete(fmt.Sprintf("webhooks/%d", id), url.Values{"force": []string{"true"}}, &result)
	return
}

// WebhookRetrieveDelivery retrieves a webhook delivery object
func (c *Client) WebhookRetrieveDelivery(id, delivery int) (result WebhookDelivery, err error) {
	err = c.Retrieve(fmt.Sprintf("/wp-json/wc/v2/webhooks/%d/deliveries/%d", id, delivery), nil, result)
	return
}

// WebhookRetrieveDeliveries retrieves all webhook delivery objects
func (c *Client) WebhookRetrieveDeliveries(id int) (result []WebhookDelivery, err error) {
	err = c.Retrieve(fmt.Sprintf("/wp-json/wc/v2/webhooks/%d/deliveries", id), nil, result)
	return
}

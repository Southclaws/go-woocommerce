// Package woocommerce provides API functions to access the WooCommerce REST API v3
package woocommerce

import (
	"context"
	"strings"

	"github.com/pkg/errors"
	"github.com/robbiet480/go-wordpress"
	"gopkg.in/resty.v1"
)

// Config stores endpoint and credentials
type Config struct {
	Endpoint    string `required:"true" envconfig:"ENDPOINT"`     // WordPress
	ConsumerKey string `required:"true" envconfig:"CONSUMER_KEY"` // WooCommerce
	SecretKey   string `required:"true" envconfig:"SECRET_KEY"`   // WooCommerce
	Username    string `required:"true" envconfig:"USERNAME"`     // WordPress
	Password    string `required:"true" envconfig:"PASSWORD"`     // WordPress
}

// Client provides an interface to the WooCommerce API via Resty
type Client struct {
	r  *resty.Client
	wp *wordpress.Client
}

// New creates a new client with the specified credentials
func New(config Config) (client *Client, err error) {
	if !strings.HasSuffix(config.Endpoint, "/") {
		config.Endpoint += "/"
	}
	client = &Client{
		r: resty.New().
			SetHostURL(config.Endpoint).
			SetRESTMode().
			SetBasicAuth(config.ConsumerKey, config.SecretKey),
	}

	_, err = client.r.R().Get("/wp-json/wc/v2/system_status")
	if err != nil {
		err = errors.Wrap(err, "failed to access WooCommerce API")
		return
	}

	client.wp, err = wordpress.NewClient(
		config.Endpoint,
		(&wordpress.BasicAuthTransport{
			Username: config.Username,
			Password: config.Password,
		}).Client())
	if err != nil {
		err = errors.Wrap(err, "failed to create wordpress client")
	}

	_, _, err = client.wp.Settings.List(context.Background())
	if err != nil {
		err = errors.Wrap(err, "failed to access WordPress API")
		return
	}

	return
}

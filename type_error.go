package woocommerce

import (
	"fmt"
)

// Error reflects http://woocommerce.github.io/woocommerce-rest-api-docs/#errors
type Error struct {
	Code    string                 `json:"code"`    // Error code
	Message string                 `json:"message"` // Error message string
	Data    map[string]interface{} `json:"data"`    // Misc metadata
}

// Error satisfies the error interface
func (e Error) Error() string {
	return fmt.Sprintf("%s: %s (%s)", e.Code, e.Message, e.Data)
}

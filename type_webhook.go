package woocommerce

import "github.com/joeshaw/iso8601"

// Status represents a webhook status
type Status string

// - active (delivers payload)
// - paused (delivery paused by admin)
// - disabled (delivery paused by failure)
const (
	StatusActive   Status = "active"
	StatusPaused   Status = "paused"
	StatusDisabled Status = "disabled"
)

// Topic reflects http://woocommerce.github.io/woocommerce-rest-api-docs/#topics
type Topic string

// The topic is a combination resource (e.g. order) and event (e.g. created) and
// maps to one or more hook names (e.g. woocommerce_checkout_order_processed).
// Webhooks can be created using the topic name and the appropriate hooks are
// automatically added.
const (
	TopicCouponCreated   Topic = "coupon.created"
	TopicCouponUpdated   Topic = "coupon.updated"
	TopicCouponDeleted   Topic = "coupon.deleted"
	TopicCustomerCreated Topic = "customer.created"
	TopicCustomerUpdated Topic = "customer.updated"
	TopicCustomerDeleted Topic = "customer.deleted"
	TopicOrderCreated    Topic = "order.created"
	TopicOrderUpdated    Topic = "order.updated"
	TopicOrderDeleted    Topic = "order.deleted"
	TopicProductCreated  Topic = "product.created"
	TopicProductUpdated  Topic = "product.updated"
	TopicProductDeleted  Topic = "product.deleted"
	TopicCustom          Topic = "action"
)

// Webhook reflects http://woocommerce.github.io/woocommerce-rest-api-docs/?shell#webhook-properties
// nolint:lll
type Webhook struct {
	ID              *int          `json:"id,omitempty"`                // Unique identifier for the resource.
	Name            *string       `json:"name,omitempty"`              // A friendly name for the webhook.
	Status          *Status       `json:"status,omitempty"`            // Webhook status. Options: active, paused and disabled. Default is active.
	Topic           *Topic        `json:"topic,omitempty"`             // Webhook topic.
	Resource        *string       `json:"resource,omitempty"`          // Webhook resource.
	Event           *string       `json:"event,omitempty"`             // Webhook event.
	Hooks           []string      `json:"hooks,omitempty"`             // WooCommerce action names associated with the webhook.
	DeliveryURL     *string       `json:"delivery_url,omitempty"`      // The URL where the webhook payload is delivered.
	Secret          *string       `json:"secret,omitempty"`            // Secret key used to generate a hash of the delivered webhook and provided in the request headers. This will default is a MD5 hash from the current user’s ID
	DateCreated     *iso8601.Time `json:"date_created,omitempty"`      // The date the webhook was created, in the site’s timezone.
	DateCreatedGmt  *iso8601.Time `json:"date_created_gmt,omitempty"`  // The date the webhook was created, as GMT.
	DateModified    *iso8601.Time `json:"date_modified,omitempty"`     // The date the webhook was last modified, in the site’s timezone.
	DateModifiedGmt *iso8601.Time `json:"date_modified_gmt,omitempty"` // The date the webhook was last modified, as GMT.
}

// WebhookDelivery reflects http://woocommerce.github.io/woocommerce-rest-api-docs/#webhook-properties
type WebhookDelivery struct {
	ID              *int                   `json:"id,omitempty"`               // Unique identifier for the resource.
	Duration        *string                `json:"duration,omitempty"`         // The delivery duration, in seconds.
	Summary         *string                `json:"summary,omitempty"`          // A friendly summary of the response including the HTTP response code, message, and body.
	RequestURL      *string                `json:"request_url,omitempty"`      // The URL where the webhook was delivered.
	RequestHeaders  map[string]interface{} `json:"request_headers,omitempty"`  // Request headers.
	RequestBody     *string                `json:"request_body,omitempty"`     // Request body.
	ResponseCode    *string                `json:"response_code,omitempty"`    // The HTTP response code from the receiving server.
	ResponseMessage *string                `json:"response_message,omitempty"` // The HTTP response message from the receiving server.
	ResponseHeaders map[string]interface{} `json:"response_headers,omitempty"` // Array of the response headers from the receiving server.
	ResponseBody    string                 `json:"response_body,omitempty"`    // The response body from the receiving server.
	DateCreated     *iso8601.Time          `json:"date_created,omitempty"`     // The date the webhook delivery was logged, in the site's timezone.
	DateCreatedGmt  *iso8601.Time          `json:"date_created_gmt,omitempty"` // The date the webhook delivery was logged, GMT.
}

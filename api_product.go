package woocommerce

import (
	"fmt"
	"net/url"
	"strconv"

	"github.com/Southclaws/qstring"
	"github.com/pkg/errors"
)

// ProductCreate reflects http://woocommerce.github.io/woocommerce-rest-api-docs/?shell#create-a-product
func (c *Client) ProductCreate(product Product) (result Product, err error) {
	err = c.Create("products", nil, product, &result)
	return
}

// ProductGet reflects http://woocommerce.github.io/woocommerce-rest-api-docs/?shell#retrieve-a-product
func (c *Client) ProductGet(id int) (result Product, err error) {
	err = c.Retrieve("products", nil, &result)
	return
}

// ProductListParams defines query parameters for listing products
type ProductListParams struct {
	Context       string `qstring:"context,omitempty"`              // Scope under which the request is made; determines fields present in response. Options: view and edit. Default is view.
	Page          int    `qstring:"page,omitempty"`                 // Current page of the collection. Default is 1.
	PerPage       int    `qstring:"per_page,omitempty"`             // Maximum number of items to be returned in result set. Default is 10.
	Search        string `qstring:"search,omitempty"`               // Limit results to those matching a string.
	After         string `qstring:"after,omitempty"`                // Limit response to resources published after a given ISO8601 compliant date.
	Before        string `qstring:"before,omitempty"`               // Limit response to resources published before a given ISO8601 compliant date.
	Exclude       []int  `qstring:"exclude,omitempty,comma"`        // Ensure result set excludes specific IDs.
	Include       []int  `qstring:"include,omitempty,comma"`        // Limit result set to specific ids.
	Offset        int    `qstring:"offset,omitempty"`               // Offset the result set by a specific number of items.
	Order         string `qstring:"order,omitempty"`                // Order sort attribute ascending or descending. Options: asc and desc. Default is desc.
	OrderBy       string `qstring:"orderby,omitempty"`              // Sort collection by object attribute. Options: date, id, include, title and slug. Default is date.
	Parent        []int  `qstring:"parent,omitempty,comma"`         // Limit result set to those of particular parent IDs.
	ParentExclude []int  `qstring:"parent_exclude,omitempty,comma"` // Limit result set to all items except those of a particular parent ID.
	Slug          string `qstring:"slug,omitempty"`                 // Limit result set to products with a specific slug.
	Status        string `qstring:"status,omitempty"`               // Limit result set to products assigned a specific status. Options: any, draft, pending, private and publish. Default is any.
	Type          string `qstring:"type,omitempty"`                 // Limit result set to products assigned a specific type. Options: simple, grouped, external and variable.
	Sku           string `qstring:"sku,omitempty"`                  // Limit result set to products with a specific SKU.
	Featured      bool   `qstring:"featured,omitempty"`             // Limit result set to featured products.
	Category      string `qstring:"category,omitempty"`             // Limit result set to products assigned a specific category ID.
	Tag           string `qstring:"tag,omitempty"`                  // Limit result set to products assigned a specific tag ID.
	ShippingClass string `qstring:"shipping_class,omitempty"`       // Limit result set to products assigned a specific shipping class ID.
	Attribute     string `qstring:"attribute,omitempty"`            // Limit result set to products with a specific attribute.
	AttributeTerm string `qstring:"attribute_term,omitempty"`       // Limit result set to products with a specific attribute term ID (required an assigned attribute).
	TaxClass      string `qstring:"tax_class,omitempty"`            // Limit result set to products with a specific tax class. Default options: standard, reduced-rate and zero-rate.
	InStock       bool   `qstring:"in_stock,omitempty"`             // Limit result set to products in stock or out of stock.
	OnSale        bool   `qstring:"on_sale,omitempty"`              // Limit result set to products on sale.
	MinPrice      string `qstring:"min_price,omitempty"`            // Limit result set to products based on a minimum price.
	MaxPrice      string `qstring:"max_price,omitempty"`            // Limit result set to products based on a maximum price.
}

// ProductList reflects http://woocommerce.github.io/woocommerce-rest-api-docs/?shell#list-all-products
func (c *Client) ProductList(query ProductListParams) (result []Product, err error) {
	values, err := qstring.Marshal(&query)
	if err != nil {
		err = errors.Wrap(err, "failed to encode query parameters")
		return
	}
	err = c.Retrieve("products", values, &result)
	return
}

// ProductUpdate reflects http://woocommerce.github.io/woocommerce-rest-api-docs/?shell#update-a-product
func (c *Client) ProductUpdate(id int, product Product) (result Product, err error) {
	err = c.Update(fmt.Sprintf("products/%d", id), nil, product, &result)
	return
}

// ProductDelete reflects http://woocommerce.github.io/woocommerce-rest-api-docs/?shell#delete-a-product
func (c *Client) ProductDelete(id int, force bool) (result Product, err error) {
	err = c.Delete(fmt.Sprintf("products/%d", id), url.Values{"force": []string{strconv.FormatBool(force)}}, &result)
	return
}

// ProductAction represents a batch operation
type ProductAction string

// Enumerates the possible batch actions
const (
	ProductActionCreate ProductAction = "create"
	ProductActionUpdate ProductAction = "update"
	ProductActionDelete ProductAction = "delete"
)

// ProductActionBatch facilitates batch operations
type ProductActionBatch map[ProductAction][]Product

// ProductUpdateBatch reflects http://woocommerce.github.io/woocommerce-rest-api-docs/?shell#batch-update-products
func (c *Client) ProductUpdateBatch(actions ProductActionBatch) (result Product, err error) {
	err = c.Update("products/batch", nil, actions, &result)
	return
}

package woocommerce

import (
	"fmt"
	"testing"
	"time"

	"github.com/joeshaw/iso8601"
	"github.com/stretchr/testify/assert"
)

// Note: these tests depend on each other. The results of the create test are
// used for the generation and checking of the retrieve test.

var createdWebhooks []Webhook

func TestClient_WebhookCreate(t *testing.T) {
	type args struct {
		name   string
		status Status
		topic  Topic
		url    string
	}
	tests := []struct {
		args       args
		wantResult Webhook
	}{
		{args{"test1", StatusActive, TopicOrderCreated, "http://example.com"},
			Webhook{
				Name:     pS("test1"),
				Status:   pStatus(StatusActive),
				Topic:    pTopic(TopicOrderCreated),
				Resource: pS("order"),
				Event:    pS("created"),
				Hooks: []string{
					"woocommerce_process_shop_order_meta",
					"woocommerce_new_order",
				},
				DeliveryURL: pS("http://example.com"),
			}},
		{args{"test2", StatusActive, TopicOrderDeleted, "http://example.com"},
			Webhook{
				Name:     pS("test2"),
				Status:   pStatus(StatusActive),
				Topic:    pTopic(TopicOrderDeleted),
				Resource: pS("order"),
				Event:    pS("deleted"),
				Hooks: []string{
					"wp_trash_post",
				},
				DeliveryURL: pS("http://example.com"),
			}},
	}
	for _, tt := range tests {
		t.Run(string(tt.args.topic), func(t *testing.T) {
			gotResult, err := client.WebhookCreate(tt.args.name, tt.args.status, tt.args.topic, tt.args.url)
			if err != nil {
				t.Error(err)
			}

			assert.Equal(t, tt.wantResult.Name, gotResult.Name)
			assert.Equal(t, tt.wantResult.Status, gotResult.Status)
			assert.Equal(t, tt.wantResult.Topic, gotResult.Topic)
			assert.Equal(t, tt.wantResult.Resource, gotResult.Resource)
			assert.Equal(t, tt.wantResult.Event, gotResult.Event)
			assert.Equal(t, tt.wantResult.Hooks, gotResult.Hooks)
			assert.Equal(t, tt.wantResult.DeliveryURL, gotResult.DeliveryURL)

			createdWebhooks = append(createdWebhooks, gotResult)
		})
	}
}

func TestClient_WebhookRetrieve(t *testing.T) {
	type args struct {
		id int
	}
	tests := []struct {
		args       args
		wantResult Webhook
	}{
		{args{*createdWebhooks[0].ID}, createdWebhooks[0]},
		{args{*createdWebhooks[1].ID}, createdWebhooks[1]},
	}
	for _, tt := range tests {
		t.Run(fmt.Sprint(tt.args.id), func(t *testing.T) {
			gotResult, err := client.WebhookRetrieve(tt.args.id)
			if err != nil {
				t.Error(err)
			}

			gotResult.DateModified = tt.wantResult.DateModified
			gotResult.DateModifiedGmt = tt.wantResult.DateModifiedGmt

			assert.Equal(t, tt.wantResult, gotResult)
		})
	}
}

func TestClient_WebhookList(t *testing.T) {
	ids := make([]int, len(createdWebhooks))
	for i, w := range createdWebhooks {
		ids[i] = *w.ID
	}

	type args struct {
		query WebhookListParams
	}
	tests := []struct {
		args       args
		wantResult []Webhook
	}{
		{args{WebhookListParams{
			Include: ids,
			PerPage: 1,
			Order:   "asc",
			OrderBy: "id",
			Offset:  1,
		}}, createdWebhooks[0:1]},
		{args{WebhookListParams{
			Include: ids,
			PerPage: 2,
			Order:   "asc",
			OrderBy: "id",
		}}, createdWebhooks},
	}
	for _, tt := range tests {
		t.Run(fmt.Sprint(tt.args.query), func(t *testing.T) {
			gotResult, err := client.WebhookList(tt.args.query)
			if err != nil {
				t.Error(err)
			}

			assert.Equal(t, len(tt.wantResult), len(gotResult))

			// correct for missing params not seen in the create call
			for i := range gotResult {
				gotResult[i].DateModified = tt.wantResult[i].DateModified
				gotResult[i].DateModifiedGmt = tt.wantResult[i].DateModifiedGmt
			}

			assert.Equal(t, tt.wantResult, gotResult)
		})
	}
}

func TestClient_WebhookUpdate(t *testing.T) {
	type args struct {
		id      int
		webhook Webhook
	}
	tests := []struct {
		args       args
		wantResult Webhook
	}{
		{args{*createdWebhooks[0].ID, Webhook{
			Name: pS("updated"),
		}}, Webhook{
			Name: pS("updated"),
		}},
		// date created should not be updatable
		{args{*createdWebhooks[1].ID, Webhook{
			DateCreated: pT(iso8601.New(time.Date(2001, time.January, 1, 1, 1, 1, 1, time.Local))),
		}}, Webhook{
			DateCreated: createdWebhooks[1].DateCreated,
		}},
	}
	for _, tt := range tests {
		t.Run(fmt.Sprint(tt.args.id), func(t *testing.T) {
			gotResult, err := client.WebhookUpdate(tt.args.id, tt.args.webhook)
			if err != nil {
				t.Error(err)
			}

			// update our test state
			for i, w := range createdWebhooks {
				if *w.ID == *gotResult.ID {
					createdWebhooks[i] = gotResult
				}
			}

			webhookNonNilCompare(t, tt.wantResult, gotResult)
		})
	}
}

func TestClient_WebhookDelete(t *testing.T) {
	type args struct {
		id int
	}
	tests := []struct {
		args       args
		wantResult Webhook
	}{
		{args{*createdWebhooks[0].ID}, createdWebhooks[0]},
		{args{*createdWebhooks[1].ID}, createdWebhooks[1]},
	}
	for _, tt := range tests {
		t.Run(fmt.Sprint(tt.args.id), func(t *testing.T) {
			gotResult, err := client.WebhookDelete(tt.args.id)
			if err != nil {
				t.Error(err)
			}

			assert.Equal(t, tt.wantResult, gotResult)
		})
	}
}

// only compare the values that are not nil in a
// this makes update tests simple and generic
func webhookNonNilCompare(t *testing.T, a, b Webhook) {
	if a.ID != nil {
		assert.Equal(t, a.ID, b.ID)
	}
	if a.Name != nil {
		assert.Equal(t, a.Name, b.Name)
	}
	if a.Status != nil {
		assert.Equal(t, a.Status, b.Status)
	}
	if a.Topic != nil {
		assert.Equal(t, a.Topic, b.Topic)
	}
	if a.Resource != nil {
		assert.Equal(t, a.Resource, b.Resource)
	}
	if a.Event != nil {
		assert.Equal(t, a.Event, b.Event)
	}
	if a.Hooks != nil {
		assert.Equal(t, a.Hooks, b.Hooks)
	}
	if a.DeliveryURL != nil {
		assert.Equal(t, a.DeliveryURL, b.DeliveryURL)
	}
	if a.Secret != nil {
		assert.Equal(t, a.Secret, b.Secret)
	}
	if a.DateCreated != nil {
		assert.Equal(t, a.DateCreated, b.DateCreated)
	}
	if a.DateCreatedGmt != nil {
		assert.Equal(t, a.DateCreatedGmt, b.DateCreatedGmt)
	}
	if a.DateModified != nil {
		assert.Equal(t, a.DateModified, b.DateModified)
	}
	if a.DateModifiedGmt != nil {
		assert.Equal(t, a.DateModifiedGmt, b.DateModifiedGmt)
	}
}

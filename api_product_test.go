package woocommerce

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

// Note: these tests depend on each other. The results of the create test are
// used for the generation and checking of the retrieve test.

var createdProducts []Product

func TestClient_ProductCreate(t *testing.T) {
	type args struct {
		product Product
	}
	tests := []struct {
		args       args
		wantResult Product
	}{
		{
			args{Product{
				Name: pS("Test 1"),
				Slug: pS("test1"),
			}},
			Product{
				Name: pS("Test 1"),
				Slug: pS("test1"),
			}},
		{
			args{Product{
				Name: pS("Test 2"),
				Slug: pS("test2"),
			}},
			Product{
				Name: pS("Test 2"),
				Slug: pS("test2"),
			}},
	}
	for _, tt := range tests {
		t.Run(string(*tt.args.product.Name), func(t *testing.T) {
			gotResult, err := client.ProductCreate(tt.args.product)
			if err != nil {
				t.Error(err)
			}

			productNonNilCompare(t, tt.wantResult, gotResult)

			createdProducts = append(createdProducts, gotResult)
		})
	}
}

func TestClient_ProductList(t *testing.T) {
	ids := make([]int, len(createdProducts))
	for i, w := range createdProducts {
		ids[i] = *w.ID
	}

	type args struct {
		query ProductListParams
	}
	tests := []struct {
		args       args
		wantResult []Product
	}{
		{args{ProductListParams{
			Include: ids,
			PerPage: 1,
			Search:  "Test 1",
		}}, createdProducts[0:1]},
	}
	for _, tt := range tests {
		t.Run(fmt.Sprint(tt.args.query), func(t *testing.T) {
			gotResult, err := client.ProductList(tt.args.query)
			if err != nil {
				t.Error(err)
			}

			assert.Len(t, gotResult, len(tt.wantResult))
			// can't compare fully because WooCommerce (for some dumb reason)
			// responds with a very slightly different payload to the one
			// returned at the point of creation.
		})
	}
}

func TestClient_ProductDelete(t *testing.T) {
	type args struct {
		id int
	}
	tests := []struct {
		args       args
		wantResult Product
	}{
		{args{*createdProducts[0].ID},
			Product{
				Name: pS("Test 1"),
				Slug: pS("test1"),
			}},
		{args{*createdProducts[1].ID},
			Product{
				Name: pS("Test 2"),
				Slug: pS("test2"),
			}},
	}
	for _, tt := range tests {
		t.Run(fmt.Sprint(tt.args.id), func(t *testing.T) {
			gotResult, err := client.ProductDelete(tt.args.id, true)
			if err != nil {
				t.Error(err)
			}

			productNonNilCompare(t, tt.wantResult, gotResult)
		})
	}
}

// only compare the values that are not nil in a
// this makes update tests simple and generic
func productNonNilCompare(t *testing.T, a, b Product) {
	if a.ID != nil {
		assert.EqualValues(t, a.ID, b.ID)
	}
	if a.Name != nil {
		assert.EqualValues(t, a.Name, b.Name)
	}
	if a.Slug != nil {
		assert.EqualValues(t, a.Slug, b.Slug)
	}
	if a.Permalink != nil {
		assert.EqualValues(t, a.Permalink, b.Permalink)
	}
	if a.DateCreated != nil {
		assert.EqualValues(t, a.DateCreated, b.DateCreated)
	}
	if a.DateCreatedGMT != nil {
		assert.EqualValues(t, a.DateCreatedGMT, b.DateCreatedGMT)
	}
	if a.DateModified != nil {
		assert.EqualValues(t, a.DateModified, b.DateModified)
	}
	if a.DateModifiedGMT != nil {
		assert.EqualValues(t, a.DateModifiedGMT, b.DateModifiedGMT)
	}
	if a.ProductType != nil {
		assert.EqualValues(t, a.ProductType, b.ProductType)
	}
	if a.Status != nil {
		assert.EqualValues(t, a.Status, b.Status)
	}
	if a.Featured != nil {
		assert.EqualValues(t, a.Featured, b.Featured)
	}
	if a.CatalogVisibility != nil {
		assert.EqualValues(t, a.CatalogVisibility, b.CatalogVisibility)
	}
	if a.Description != nil {
		assert.EqualValues(t, a.Description, b.Description)
	}
	if a.ShortDescription != nil {
		assert.EqualValues(t, a.ShortDescription, b.ShortDescription)
	}
	if a.SKU != nil {
		assert.EqualValues(t, a.SKU, b.SKU)
	}
	if a.Price != nil {
		assert.EqualValues(t, a.Price, b.Price)
	}
	if a.RegularPrice != nil {
		assert.EqualValues(t, a.RegularPrice, b.RegularPrice)
	}
	if a.SalePrice != nil {
		assert.EqualValues(t, a.SalePrice, b.SalePrice)
	}
	if a.DateOnSaleFrom != nil {
		assert.EqualValues(t, a.DateOnSaleFrom, b.DateOnSaleFrom)
	}
	if a.DateOnSaleFromGMT != nil {
		assert.EqualValues(t, a.DateOnSaleFromGMT, b.DateOnSaleFromGMT)
	}
	if a.DateOnSaleTo != nil {
		assert.EqualValues(t, a.DateOnSaleTo, b.DateOnSaleTo)
	}
	if a.DateOnSaleToGMT != nil {
		assert.EqualValues(t, a.DateOnSaleToGMT, b.DateOnSaleToGMT)
	}
	if a.PriceHTML != nil {
		assert.EqualValues(t, a.PriceHTML, b.PriceHTML)
	}
	if a.OnSale != nil {
		assert.EqualValues(t, a.OnSale, b.OnSale)
	}
	if a.Purchasable != nil {
		assert.EqualValues(t, a.Purchasable, b.Purchasable)
	}
	if a.TotalSales != nil {
		assert.EqualValues(t, a.TotalSales, b.TotalSales)
	}
	if a.Virtual != nil {
		assert.EqualValues(t, a.Virtual, b.Virtual)
	}
	if a.Downloadable != nil {
		assert.EqualValues(t, a.Downloadable, b.Downloadable)
	}
	if a.Downloads != nil {
		assert.EqualValues(t, a.Downloads, b.Downloads)
	}
	if a.DownloadLimit != nil {
		assert.EqualValues(t, a.DownloadLimit, b.DownloadLimit)
	}
	if a.DownloadExpiry != nil {
		assert.EqualValues(t, a.DownloadExpiry, b.DownloadExpiry)
	}
	if a.ExternalURL != nil {
		assert.EqualValues(t, a.ExternalURL, b.ExternalURL)
	}
	if a.ButtonText != nil {
		assert.EqualValues(t, a.ButtonText, b.ButtonText)
	}
	if a.TaxStatus != nil {
		assert.EqualValues(t, a.TaxStatus, b.TaxStatus)
	}
	if a.TaxClass != nil {
		assert.EqualValues(t, a.TaxClass, b.TaxClass)
	}
	if a.ManageStock != nil {
		assert.EqualValues(t, a.ManageStock, b.ManageStock)
	}
	if a.StockQuantity != nil {
		assert.EqualValues(t, a.StockQuantity, b.StockQuantity)
	}
	if a.InStock != nil {
		assert.EqualValues(t, a.InStock, b.InStock)
	}
	if a.Backorders != nil {
		assert.EqualValues(t, a.Backorders, b.Backorders)
	}
	if a.BackordersAllowed != nil {
		assert.EqualValues(t, a.BackordersAllowed, b.BackordersAllowed)
	}
	if a.Backordered != nil {
		assert.EqualValues(t, a.Backordered, b.Backordered)
	}
	if a.SoldIndividually != nil {
		assert.EqualValues(t, a.SoldIndividually, b.SoldIndividually)
	}
	if a.Weight != nil {
		assert.EqualValues(t, a.Weight, b.Weight)
	}
	if a.Dimensions != nil {
		assert.EqualValues(t, a.Dimensions, b.Dimensions)
	}
	if a.ShippingRequired != nil {
		assert.EqualValues(t, a.ShippingRequired, b.ShippingRequired)
	}
	if a.ShippingTaxable != nil {
		assert.EqualValues(t, a.ShippingTaxable, b.ShippingTaxable)
	}
	if a.ShippingClass != nil {
		assert.EqualValues(t, a.ShippingClass, b.ShippingClass)
	}
	if a.ShippingClassID != nil {
		assert.EqualValues(t, a.ShippingClassID, b.ShippingClassID)
	}
	if a.ReviewsAllowed != nil {
		assert.EqualValues(t, a.ReviewsAllowed, b.ReviewsAllowed)
	}
	if a.AverageRating != nil {
		assert.EqualValues(t, a.AverageRating, b.AverageRating)
	}
	if a.RatingCount != nil {
		assert.EqualValues(t, a.RatingCount, b.RatingCount)
	}
	if a.RelatedIDs != nil {
		assert.EqualValues(t, a.RelatedIDs, b.RelatedIDs)
	}
	if a.UpsellIDs != nil {
		assert.EqualValues(t, a.UpsellIDs, b.UpsellIDs)
	}
	if a.CrossSellIDs != nil {
		assert.EqualValues(t, a.CrossSellIDs, b.CrossSellIDs)
	}
	if a.ParentID != nil {
		assert.EqualValues(t, a.ParentID, b.ParentID)
	}
	if a.PurchaseNote != nil {
		assert.EqualValues(t, a.PurchaseNote, b.PurchaseNote)
	}
	if a.Categories != nil {
		assert.EqualValues(t, a.Categories, b.Categories)
	}
	if a.Tags != nil {
		assert.EqualValues(t, a.Tags, b.Tags)
	}
	if a.Images != nil {
		assert.EqualValues(t, a.Images, b.Images)
	}
	if a.Attributes != nil {
		assert.EqualValues(t, a.Attributes, b.Attributes)
	}
	if a.DefaultAttributes != nil {
		assert.EqualValues(t, a.DefaultAttributes, b.DefaultAttributes)
	}
	if a.Variations != nil {
		assert.EqualValues(t, a.Variations, b.Variations)
	}
	if a.GroupedProducts != nil {
		assert.EqualValues(t, a.GroupedProducts, b.GroupedProducts)
	}
	if a.MenuOrder != nil {
		assert.EqualValues(t, a.MenuOrder, b.MenuOrder)
	}
	if a.MetaData != nil {
		assert.EqualValues(t, a.MetaData, b.MetaData)
	}
}

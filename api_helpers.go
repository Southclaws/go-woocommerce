package woocommerce

import (
	"encoding/json"
	"net/http"
	"net/url"
	"path/filepath"
	"reflect"

	"github.com/pkg/errors"
	"gitlab.com/Southclaws/go-woocommerce/util"
)

// ErrMustBePointer is returned when result parameter is not a pointer
var ErrMustBePointer = errors.New("result must be pointer to struct")

// Retrieve is a generic GET method
func (c *Client) Retrieve(path string, query url.Values, result interface{}) (err error) {
	if reflect.ValueOf(result).Kind() != reflect.Ptr {
		return ErrMustBePointer
	}

	u, err := url.Parse(filepath.Join("/wp-json/wc/v2/", path))
	if err != nil {
		return
	}
	if query != nil {
		u.RawQuery = query.Encode()
	}

	response, err := c.r.R().
		SetResult(&result).
		Get(u.String())
	if err != nil {
		return
	}

	if response.StatusCode() >= 400 && response.StatusCode() <= 500 {
		e := Error{}
		err = json.Unmarshal(response.Body(), &e)
		if err != nil {
			return errors.Wrap(err, "failed to decode response as error")
		}
		return e
	}

	return
}

// Create is a generic POST method
func (c *Client) Create(path string, query url.Values, body interface{}, result interface{}) (err error) {
	if reflect.ValueOf(result).Kind() != reflect.Ptr {
		return ErrMustBePointer
	}

	u, err := url.Parse(filepath.Join("/wp-json/wc/v2/", path))
	if err != nil {
		return
	}
	if query != nil {
		u.RawQuery = query.Encode()
	}

	response, err := c.r.R().
		SetBody(body).
		SetResult(&result).
		Post(u.String())
	if err != nil {
		return
	}

	if err = util.AssertStatus(response.StatusCode(), http.StatusCreated); err != nil {
		e := Error{}
		err = json.Unmarshal(response.Body(), &e)
		if err != nil {
			return errors.Wrap(err, "failed to decode response as error")
		}
		return e
	}

	return
}

// Update is a generic PUT method
func (c *Client) Update(path string, query url.Values, body interface{}, result interface{}) (err error) {
	if reflect.ValueOf(result).Kind() != reflect.Ptr {
		return ErrMustBePointer
	}

	u, err := url.Parse(filepath.Join("/wp-json/wc/v2/", path))
	if err != nil {
		return
	}
	if query != nil {
		u.RawQuery = query.Encode()
	}

	response, err := c.r.R().
		SetBody(body).
		SetResult(&result).
		Put(u.String())
	if err != nil {
		return
	}

	if response.StatusCode() >= 400 && response.StatusCode() <= 500 {
		e := Error{}
		err = json.Unmarshal(response.Body(), &e)
		if err != nil {
			return errors.Wrap(err, "failed to decode response as error")
		}
		return e
	}

	return
}

// Delete is a generic DELETE method
func (c *Client) Delete(path string, query url.Values, result interface{}) (err error) {
	if reflect.ValueOf(result).Kind() != reflect.Ptr {
		return ErrMustBePointer
	}

	u, err := url.Parse(filepath.Join("/wp-json/wc/v2/", path))
	if err != nil {
		return
	}
	if query != nil {
		u.RawQuery = query.Encode()
	}

	response, err := c.r.R().
		SetResult(&result).
		Delete(u.String())
	if err != nil {
		return
	}

	if err = util.AssertStatus(response.StatusCode(), http.StatusOK); err != nil {
		e := Error{}
		err = json.Unmarshal(response.Body(), &e)
		if err != nil {
			return errors.Wrap(err, "failed to decode response as error")
		}
		return e
	}

	return
}

package woocommerce

// nolint:lll

import (
	"github.com/joeshaw/iso8601"
)

// Product reflects http://woocommerce.github.io/woocommerce-rest-api-docs/#product-properties
type Product struct {
	ID                *int                           `json:"id,omitempty"`                    // Unique identifier for the resource.
	Name              *string                        `json:"name,omitempty"`                  // Product name.
	Slug              *string                        `json:"slug,omitempty"`                  // Product slug.
	Permalink         *string                        `json:"permalink,omitempty"`             // Product URL.
	DateCreated       *iso8601.Time                  `json:"date_created,omitempty"`          // The date the product was created, in the site’s timezone.
	DateCreatedGMT    *iso8601.Time                  `json:"date_created_gmt,omitempty"`      // The date the product was created, as GMT.
	DateModified      *iso8601.Time                  `json:"date_modified,omitempty"`         // The date the product was last modified, in the site’s timezone.
	DateModifiedGMT   *iso8601.Time                  `json:"date_modified_gmt,omitempty"`     // The date the product was last modified, as GMT.
	ProductType       *string                        `json:"type,omitempty"`                  // Product type. Options: simple, grouped, external and variable. Default is simple.
	Status            *string                        `json:"status,omitempty"`                // Product status (post status). Options: draft, pending, private and publish. Default is publish.
	Featured          *bool                          `json:"featured,omitempty"`              // Featured product. Default is false.
	CatalogVisibility *string                        `json:"catalog_visibility,omitempty"`    // Catalog visibility. Options: visible, catalog, search and hidden. Default is visible.
	Description       *string                        `json:"description,omitempty"`           // Product description.
	ShortDescription  *string                        `json:"short_description,omitempty"`     // Product short description.
	SKU               *string                        `json:"sku,omitempty"`                   // Unique identifier.
	Price             *string                        `json:"price,omitempty"`                 // Current product price.
	RegularPrice      *string                        `json:"regular_price,omitempty"`         // Product regular price.
	SalePrice         *string                        `json:"sale_price,omitempty"`            // Product sale price.
	DateOnSaleFrom    *iso8601.Time                  `json:"date_on_sale_from,omitempty"`     // Start date of sale price, in the site’s timezone.
	DateOnSaleFromGMT *iso8601.Time                  `json:"date_on_sale_from_gmt,omitempty"` // Start date of sale price, as GMT.
	DateOnSaleTo      *iso8601.Time                  `json:"date_on_sale_to,omitempty"`       // End date of sale price, in the site’s timezone.
	DateOnSaleToGMT   *iso8601.Time                  `json:"date_on_sale_to_gmt,omitempty"`   // End date of sale price, as GMT.
	PriceHTML         *string                        `json:"price_html,omitempty"`            // Price formatted in HTML.
	OnSale            *bool                          `json:"on_sale,omitempty"`               // Shows if the product is on sale.
	Purchasable       *bool                          `json:"purchasable,omitempty"`           // Shows if the product can be bought.
	TotalSales        *int                           `json:"total_sales,omitempty"`           // Amount of sales.
	Virtual           *bool                          `json:"virtual,omitempty"`               // If the product is virtual. Default is false.
	Downloadable      *bool                          `json:"downloadable,omitempty"`          // If the product is downloadable. Default is false.
	Downloads         []Download                     `json:"downloads,omitempty"`             // List of downloadable files. See Product - Downloads properties
	DownloadLimit     *int                           `json:"download_limit,omitempty"`        // Number of times downloadable files can be downloaded after purchase. Default is -1.
	DownloadExpiry    *int                           `json:"download_expiry,omitempty"`       // Number of days until access to downloadable files expires. Default is -1.
	ExternalURL       *string                        `json:"external_url,omitempty"`          // Product external URL. Only for external products.
	ButtonText        *string                        `json:"button_text,omitempty"`           // Product external button text. Only for external products.
	TaxStatus         *string                        `json:"tax_status,omitempty"`            // Tax status. Options: taxable, shipping and none. Default is taxable.
	TaxClass          *string                        `json:"tax_class,omitempty"`             // Tax class.
	ManageStock       *bool                          `json:"manage_stock,omitempty"`          // Stock management at product level. Default is false.
	StockQuantity     *int                           `json:"stock_quantity,omitempty"`        // Stock quantity.
	InStock           *bool                          `json:"in_stock,omitempty"`              // Controls whether or not the product is listed as “in stock” or “out of stock” on the frontend. Default is true.
	Backorders        *string                        `json:"backorders,omitempty"`            // If managing stock, this controls if backorders are allowed. Options: no, notify and yes. Default is no.
	BackordersAllowed *bool                          `json:"backorders_allowed,omitempty"`    // Shows if backorders are allowed.
	Backordered       *bool                          `json:"backordered,omitempty"`           // Shows if the product is on backordered.
	SoldIndividually  *bool                          `json:"sold_individually,omitempty"`     // Allow one item to be bought in a single order. Default is false.
	Weight            *string                        `json:"weight,omitempty"`                // Product weight.
	Dimensions        *Dimensions                    `json:"dimensions,omitempty"`            // object 	// Product dimensions. See Product - Dimensions properties
	ShippingRequired  *bool                          `json:"shipping_required,omitempty"`     // Shows if the product need to be shipped.
	ShippingTaxable   *bool                          `json:"shipping_taxable,omitempty"`      // Shows whether or not the product shipping is taxable.
	ShippingClass     *string                        `json:"shipping_class,omitempty"`        // Shipping class slug.
	ShippingClassID   *int                           `json:"shipping_class_id,omitempty"`     // Shipping class ID.
	ReviewsAllowed    *bool                          `json:"reviews_allowed,omitempty"`       // Allow reviews. Default is true.
	AverageRating     *string                        `json:"average_rating,omitempty"`        // Reviews average rating.
	RatingCount       *int                           `json:"rating_count,omitempty"`          // Amount of reviews that the product have.
	RelatedIDs        []int                          `json:"related_ids,omitempty"`           // List of related products IDs.
	UpsellIDs         []int                          `json:"upsell_ids,omitempty"`            // List of up-sell products IDs.
	CrossSellIDs      []int                          `json:"cross_sell_ids,omitempty"`        // List of cross-sell products IDs.
	ParentID          *int                           `json:"parent_id,omitempty"`             // Product parent ID.
	PurchaseNote      *string                        `json:"purchase_note,omitempty"`         // Optional note to send the customer after purchase.
	Categories        []Category                     `json:"categories,omitempty"`            // List of categories. See Product - Categories properties
	Tags              []Tag                          `json:"tags,omitempty"`                  // List of tags. See Product - Tags properties
	Images            []Image                        `json:"images,omitempty"`                // List of images. See Product - Images properties
	Attributes        []Attribute                    `json:"attributes,omitempty"`            // List of attributes. See Product - Attributes properties
	DefaultAttributes []DefaultAttribute             `json:"default_attributes,omitempty"`    // Defaults variation attributes. See Product - Default attributes properties
	Variations        []int                          `json:"variations,omitempty"`            // List of variations IDs.
	GroupedProducts   []int                          `json:"grouped_products,omitempty"`      // List of grouped products ID.
	MenuOrder         *int                           `json:"menu_order,omitempty"`            // Menu order, used to custom sort products.
	MetaData          []Metadata                     `json:"meta_data,omitempty"`             // Meta data. See Product - Meta data properties
	Links             map[string][]map[string]string `json:"_links,omitempty"`
}

// Products is a collection of product objects for sorting
type Products []Product

func (p Products) Len() int           { return len(p) }
func (p Products) Less(i, j int) bool { return *p[i].ID < *p[j].ID }
func (p Products) Swap(i, j int)      { p[i], p[j] = p[j], p[i] }

// ByName implements the sort interface for product name
type ByName Products

func (p ByName) Len() int           { return len(p) }
func (p ByName) Less(i, j int) bool { return *p[i].Name < *p[j].Name }
func (p ByName) Swap(i, j int)      { p[i], p[j] = p[j], p[i] }

// BySKU implements the sort interface for product SKU
type BySKU Products

func (p BySKU) Len() int           { return len(p) }
func (p BySKU) Less(i, j int) bool { return *p[i].SKU < *p[j].SKU }
func (p BySKU) Swap(i, j int)      { p[i], p[j] = p[j], p[i] }

// Download reflects http://woocommerce.github.io/woocommerce-rest-api-docs/?shell#product-downloads-properties
type Download struct {
	ID   *string `json:"id"`   // File MD5 hash.
	Name *string `json:"name"` // File name.
	File *string `json:"file"` // File URL.
}

// Dimensions reflects http://woocommerce.github.io/woocommerce-rest-api-docs/?shell#product-dimensions-properties
type Dimensions struct {
	Length *string `json:"length"` // Product length.
	Width  *string `json:"width"`  // Product width.
	Height *string `json:"height"` // Product height.
}

// Category reflects http://woocommerce.github.io/woocommerce-rest-api-docs/?shell#product-categories-properties
type Category struct {
	ID   *int    `json:"id"`   // Category ID.
	Name *string `json:"name"` // Category name.
	Slug *string `json:"slug"` // Category slug.
}

// Tag reflects http://woocommerce.github.io/woocommerce-rest-api-docs/?shell#product-tags-properties
type Tag struct {
	ID   *int    `json:"id"`   // Tag ID.
	Name *string `json:"name"` // Tag name.
	Slug *string `json:"slug"` // Tag slug.
}

// Image reflects http://woocommerce.github.io/woocommerce-rest-api-docs/?shell#product-images-properties
type Image struct {
	ID              *int          `json:"id"`                // Image ID.
	DateCreated     *iso8601.Time `json:"date_created"`      // The date the image was created, in the site’s timezone.
	DateCreatedGmt  *iso8601.Time `json:"date_created_gmt"`  // The date the image was created, as GMT.
	DateModified    *iso8601.Time `json:"date_modified"`     // The date the image was last modified, in the site’s timezone.
	DateModifiedGmt *iso8601.Time `json:"date_modified_gmt"` // The date the image was last modified, as GMT.
	Src             *string       `json:"src"`               // Image URL.
	Name            *string       `json:"name"`              // Image name.
	Alt             *string       `json:"alt"`               // Image alternative text.
	Position        *int          `json:"position"`          // Image position. 0 means that the image is featured.
}

// Attribute reflects http://woocommerce.github.io/woocommerce-rest-api-docs/?shell#product-attributes-properties
type Attribute struct {
	ID        *int     `json:"id"`        // Attribute ID.
	Name      *string  `json:"name"`      // Attribute name.
	Position  *int     `json:"position"`  // Attribute position.
	Visible   *bool    `json:"visible"`   // Define if the attribute is visible on the “Additional information” tab in the product’s page. Default is false.
	Variation *bool    `json:"variation"` // Define if the attribute can be used as variation. Default is false.
	Options   []string `json:"options"`   // List of available term names of the attribute.
}

// DefaultAttribute reflects http://woocommerce.github.io/woocommerce-rest-api-docs/?shell#product-default-attributes-properties
type DefaultAttribute struct {
	ID     *int    `json:"id"`     // Attribute ID.
	Name   *string `json:"name"`   // Attribute name.
	Option *string `json:"option"` // Selected attribute term name.
}

// Metadata reflects http://woocommerce.github.io/woocommerce-rest-api-docs/?shell#product-metadata-properties
type Metadata struct {
	ID    *int    `json:"id"`    // Meta ID.
	Key   *string `json:"key"`   // Meta key.
	Value *string `json:"value"` // Meta value.
}

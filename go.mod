module gitlab.com/Southclaws/go-woocommerce

require (
	github.com/PuerkitoBio/goquery v1.4.1 // indirect
	github.com/Southclaws/qstring v0.0.0-20181021182432-a88b87ed8fc0
	github.com/andybalholm/cascadia v1.0.0 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/dyninc/qstring v0.0.0-20160719172318-ab5840a88e81 // indirect
	github.com/google/go-querystring v1.0.0 // indirect
	github.com/joeshaw/iso8601 v0.0.0-20140327141645-861d1ce636d0
	github.com/joho/godotenv v1.3.0
	github.com/kelseyhightower/envconfig v1.3.0
	github.com/pkg/errors v0.8.0
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/robbiet480/go-wordpress v0.0.0-20180206201500-3b8369ffcef3
	github.com/stretchr/testify v1.2.1
	github.com/tomnomnom/linkheader v0.0.0-20180905144013-02ca5825eb80 // indirect
	gopkg.in/resty.v1 v1.9.1
)
